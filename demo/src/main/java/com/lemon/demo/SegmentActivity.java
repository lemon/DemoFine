package com.lemon.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.lemon.segmentcontrol.SegmentItemView;
import com.lemon.segmentcontrol.SegmentView;
import com.lemon.stepview.StepView;

/**
 * Created by Administrator on 2015/11/22.
 */
public class SegmentActivity extends AppCompatActivity implements SegmentView.OnItemClickListener {

    private String[] mText1 = {"item1", "item2"};

    private String[] mText2 = {"item1", "item2", "item3"};

    private String[] mText3 = {"item1", "item2", "item3", "item4"};

    private SegmentView segmentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segment_switch);

        segmentView = (SegmentView) findViewById(R.id.segmentView);
        segmentView.setItems(mText1);
        segmentView.setOnItemClickListener(this);

        segmentView = (SegmentView) findViewById(R.id.segmentView1);
        segmentView.setItems(mText2);
        segmentView.setOnItemClickListener(this);

        segmentView = (SegmentView) findViewById(R.id.segmentView2);
        segmentView.setItems(mText2);
        segmentView.setOnItemClickListener(this);

        segmentView = (SegmentView) findViewById(R.id.segmentView3);
        segmentView.setItems(mText3);
        segmentView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(SegmentItemView item, int checkedItem) {
        Toast.makeText(SegmentActivity.this,"checkedItem == " + checkedItem, Toast.LENGTH_SHORT).show();
    }
}

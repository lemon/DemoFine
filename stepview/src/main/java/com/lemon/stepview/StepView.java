package com.lemon.stepview;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class StepView extends LinearLayout implements StepIndicatorView.OnDrawListener {

    private StepIndicatorView mStepIndicatorView;
    private FrameLayout mLabelsContainerLayout;
    private String[] mLabels;
    private int mCompletedPosition;
    private int mLabelColor = Color.BLACK;
    private int mDefaultColor = Color.BLACK;
    private int mSelectedColor = Color.YELLOW;

    public StepView(Context context) {
        this(context, null);
    }

    public StepView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StepView);
        mDefaultColor = typedArray.getColor(R.styleable.StepView_defaultColor, Color.BLACK);
        mSelectedColor = typedArray.getColor(R.styleable.StepView_indicatorColor, Color.BLUE);
        mLabelColor = typedArray.getColor(R.styleable.StepView_labelColor, Color.BLUE);
        typedArray.recycle();
        init();
    }

    private void init() {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.widget_step_view, this);
        mStepIndicatorView = (StepIndicatorView) rootView.findViewById(R.id.step_indicator_view);
        mStepIndicatorView.setDrawListener(this);
        mLabelsContainerLayout = (FrameLayout) rootView.findViewById(R.id.labels_container_layout);

        mStepIndicatorView.setColor(mDefaultColor);
        mStepIndicatorView.setIndicatorColor(mSelectedColor);
    }

    public StepView setLabels(String[] labels) {
        mLabels = labels;
        mStepIndicatorView.setStepSize(mLabels.length);
        return this;
    }

    public int getCompletedPosition() {
        return mCompletedPosition;
    }

    public StepView setCompletedPosition(int completedPosition) {
        mCompletedPosition = completedPosition;
        mStepIndicatorView.setCompletedPosition(completedPosition);
        return this;
    }

    private void drawLabels() {
        List<Float> indicatorPosition = mStepIndicatorView.getCircleXPositionContainers();

        if (mLabels != null) {
            for (int i = 0; i < mLabels.length; i++) {
                TextView textView = new TextView(getContext());
                textView.setText(mLabels[i]);
                textView.setTextColor(mLabelColor);
                textView.setX(indicatorPosition.get(i) - mStepIndicatorView.getCircleRadius());
                textView.setLayoutParams(
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if (i <= mCompletedPosition) {
                    textView.setTypeface(null, Typeface.BOLD);
                }
                mLabelsContainerLayout.addView(textView);
            }
        }
    }


    @Override
    public void onFinish() {
        drawLabels();
    }
}

package com.lemon.stepview;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class StepIndicatorView extends View {

    private static final String TAG = StepIndicatorView.class.getSimpleName();

    private int mStepSize = 2;
    private float mLineHeight;
    private float mCircleRadius;
    private float mRingCircleRadius;
    private int mDefaultColor = Color.BLACK;
    private int mSelectedColor = Color.YELLOW;
    private Paint defaultPaint = new Paint();
    private Paint selectedPaint = new Paint();

    private float mPadding;
    private float mLeftX;
    private float mLeftY;
    private float mRightX;
    private float mRightY;
    private float mCenterY;
    private float mDistance;  //间隔

    private int mCompletedPosition;
    private List<Float> mCircleXPositionContainers = new ArrayList<Float>();//圆指示器的X轴坐标集合

    private static final int DEFAULT_HEIGHT = 100;
    private static final int DEFAULT_WIDTH = 200;

    private OnDrawListener mDrawListener;

    public StepIndicatorView(Context context) {
        this(context, null);
    }

    public StepIndicatorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StepIndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        mLineHeight = DEFAULT_HEIGHT * 0.2f;
        mCircleRadius = DEFAULT_HEIGHT * 0.3f;
        mRingCircleRadius = mCircleRadius * 1.6f;
        mPadding = mRingCircleRadius;
    }

    public void setStepSize(int stepSize) {
        if (stepSize > 1) {
            this.mStepSize = stepSize;
        }
        invalidate();
    }

    public void setCompletedPosition(int position) {
        mCompletedPosition = (position < 0) ? 0 : position;
        invalidate();
    }

    public void setColor(int defaultColor) {
        this.mDefaultColor = defaultColor;
    }

    public void setIndicatorColor(int indicatorColor) {
        this.mSelectedColor = indicatorColor;
    }

    public List<Float> getCircleXPositionContainers() {
        return mCircleXPositionContainers;
    }

    public  float getCircleRadius() {
        return  mCircleRadius;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mCenterY = getHeight() / 2;
        mLeftX = mPadding;
        mLeftY = mCenterY - (mLineHeight / 2);
        mRightX = getWidth() - mPadding;
        mRightY = mCenterY + (mLineHeight / 2);
        mDistance = (mRightX - mLeftX) / (mStepSize - 1);

        mCircleXPositionContainers.add(mLeftX);
        for (int i = 1; i < mStepSize - 1; i++) {
            mCircleXPositionContainers.add(mLeftX + (i * mDistance));
        }
        mCircleXPositionContainers.add(mRightX);

        mDrawListener.onFinish();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

        System.out.println("DEFAULT_WIDTH == " + DEFAULT_WIDTH);
        System.out.println("width == " + widthSpecSize);

        System.out.println("DEFAULT_HEIGHT == " + DEFAULT_HEIGHT);
        System.out.println("hight == " + heightMeasureSpec);

        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {  //解决wrap_content的一种方法，使用默认值
            setMeasuredDimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            System.out.println("width height wrap_content");
        } else if (widthSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(DEFAULT_WIDTH, heightSpecSize);
            System.out.println("width wrap_content");
        } else if (heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, DEFAULT_HEIGHT );
            System.out.println("height wrap_content");
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        defaultPaint.setAntiAlias(true);
        defaultPaint.setColor(mDefaultColor);
        defaultPaint.setStyle(Paint.Style.STROKE);
        defaultPaint.setStrokeWidth(2);

        selectedPaint.setAntiAlias(true);
        selectedPaint.setColor(mSelectedColor);
        selectedPaint.setStyle(Paint.Style.STROKE);
        selectedPaint.setStrokeWidth(2);

        defaultPaint.setStyle(Paint.Style.FILL);
        selectedPaint.setStyle(Paint.Style.FILL);

        int size = mCircleXPositionContainers.size();

        for (int i = 0; i < size - 1; i++) {
            final float positionX = mCircleXPositionContainers.get(i);
            final float positionX2 = mCircleXPositionContainers.get(i + 1);
            canvas.drawRect(positionX, mLeftY, positionX2, mRightY,
                    (i < mCompletedPosition) ? selectedPaint : defaultPaint);
        }

        for (int i = 0; i < size; i++) {
            float positionX = mCircleXPositionContainers.get(i);
            canvas.drawCircle(positionX, mCenterY, mCircleRadius, (i <= mCompletedPosition ? selectedPaint : defaultPaint));

            if (i == mCompletedPosition) {
                selectedPaint.setColor(getColorWithAlpha(mSelectedColor, 0.3f));
                canvas.drawCircle(positionX, mCenterY, mRingCircleRadius, selectedPaint);
            }
        }
    }

    public static int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

    public void setDrawListener(OnDrawListener drawListener) {
        mDrawListener = drawListener;
    }

    public interface OnDrawListener {
        public void onFinish();
    }

}

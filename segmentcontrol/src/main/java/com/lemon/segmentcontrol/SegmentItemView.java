package com.lemon.segmentcontrol;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.TypedValue;
import android.view.View;

public class SegmentItemView extends View {

    public final static int GRAVITY_SINGLE = 1 << 0;
    public final static int GRAVITY_LEFT = 1 << 1;
    public final static int GRAVITY_CENTER = 1 << 2;
    public final static int GRAVITY_RIGHT = 1 << 3;

    private int gravity = GRAVITY_SINGLE;
    private GradientDrawable drawable = new GradientDrawable();
    private boolean isChecked;
    private GradientDrawable hideLeftBoderDrawable = new GradientDrawable();


    private String text;
    private Paint textPaint = new Paint();
    private Rect textBounds = new Rect();

    private final float r = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
    private float textSize = 5f * r;
    private int defaultColor = Color.BLUE;
    private int checkedColor = Color.WHITE;
    private int boderWidth;
    private boolean isHideLeftBoder = false;

    SegmentItemView(Context context, String text, int gravity, boolean isChecked, float textSize, int defaultColor, int checkedColor, int boderWidth,boolean isHideLeftBoder) {
        super(context);
        this.text = text;
        this.gravity = gravity;
        this.isChecked = isChecked;
        this.textSize = textSize;
        this.defaultColor = defaultColor;
        this.checkedColor = checkedColor;
        this.boderWidth = boderWidth;
        this.isHideLeftBoder = isHideLeftBoder;
        init();
    }

    private void init() {
        if (textSize <= 0.0f){
            textSize = 4.5f * r;
        }
        textPaint.setTextSize(textSize);
        textPaint.getTextBounds(text, 0, text.length(), textBounds);

        setItemGravity(gravity);
        setChecked(isChecked);
    }

    public void setItemGravity(int gravity) {
        this.gravity = gravity;
        switch (gravity) {
            case GRAVITY_SINGLE:
                drawable.setCornerRadii(new float[]{r, r, r, r, r, r, r, r});
                break;
            case GRAVITY_LEFT:
                drawable.setCornerRadii(new float[]{r, r, 0, 0, 0, 0, r, r});
                break;
            case GRAVITY_CENTER:
                drawable.setCornerRadii(new float[]{0, 0, 0, 0, 0, 0, 0, 0});
                break;
            case GRAVITY_RIGHT:
                drawable.setCornerRadii(new float[]{0, 0, r, r, r, r, 0, 0});
                break;
        }
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        textPaint.setColor(isChecked ? defaultColor : checkedColor);
        drawable.setColor(isChecked ? checkedColor : defaultColor);
        hideLeftBoderDrawable.setColor(defaultColor);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST) {
            widthSize = textBounds.width() + (int) (8 * r);
            widthMode = MeasureSpec.EXACTLY;
        }
        if (heightMode == MeasureSpec.AT_MOST) {
            heightSize = textBounds.height() + (int) (6 * r);
            heightMode = MeasureSpec.EXACTLY;
        }
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize, widthMode);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(heightSize, heightMode);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);

        int height = getMeasuredHeight();
        int width = getMeasuredWidth();
        if (height >= 0) {
            float textSize = Math.min(this.textSize, (height - 2 * r));
            if (width > 0) {
                textSize = Math.min(textSize, (width - 2 * r) * 2 / text.length()); //英文比中文短（中文为两个字符），故取mText.length()/2作为平均宽度
            }
            if (textSize != textSize) {
                textPaint.setTextSize(textSize);
                textPaint.getTextBounds(text, 0, text.length(), textBounds);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Rect rect = canvas.getClipBounds();
        drawable.setBounds(new Rect(rect));
        drawable.setStroke(boderWidth, checkedColor);
        drawable.draw(canvas);
        if (!isChecked && isHideLeftBoder) {
            Rect hideLeftBorderRect = new Rect(rect.right - boderWidth, rect.top + boderWidth, rect.right, rect.bottom - boderWidth);
            hideLeftBoderDrawable.setBounds(hideLeftBorderRect);
            hideLeftBoderDrawable.draw(canvas);
        }

        int l = (rect.width() - textBounds.width()) / 2;
        int b = (rect.height() + textBounds.height()) / 2;
        canvas.drawText(text, l, b, textPaint);
    }


}

package com.lemon.segmentcontrol;


            import android.content.Context;
    import android.content.res.TypedArray;
    import android.graphics.Color;
    import android.util.AttributeSet;
    import android.view.ViewGroup;
    import android.view.View;

    public class SegmentView extends ViewGroup implements View.OnClickListener {


//    private String[] mTextItems = {"item1", "item2", "item3"};

        private String[] mTextItems;

    private int checkedItem = 0;
    private float textSize = 0.0f;
    private int boderWidth = 0;
    private int defaultColor = Color.BLUE;
    private int checkedColor = Color.WHITE;

    private OnItemClickListener listener;

    public SegmentView(Context context) {
        this(context, null);
    }

    public SegmentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SegmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFromAttributes(context, attrs);
//        init();
    }

    protected void initFromAttributes(Context context, AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SegmentView);
        checkedItem = typedArray.getInt(R.styleable.SegmentView_checkedItem, 0);
//        textSize = typedArray.getFloat(R.styleable.SegmentView_textSize, 0.0f);
        textSize = typedArray.getDimension(R.styleable.SegmentView_textSize, 0.0f);
        defaultColor = typedArray.getColor(R.styleable.SegmentView_defaultBgColor, Color.WHITE);
        checkedColor = typedArray.getColor(R.styleable.SegmentView_checkColor, Color.BLUE);
        boderWidth = typedArray.getInt(R.styleable.SegmentView_boderWidth, 0);
        typedArray.recycle();
    }

    public void init() {
        int length = mTextItems.length;
        for (int i = 0; i < length; i++) {
            View view = new SegmentItemView(getContext(), mTextItems[i], getGravity(i, length), i == checkedItem, textSize, defaultColor, checkedColor, boderWidth, (i != length - 1));
            view.setOnClickListener(this);
            addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        }
    }


    private int getGravity(int i, int len) {
        if (i == 0) {
            if (i == len - 1) {
                return SegmentItemView.GRAVITY_SINGLE;
            } else {
                return SegmentItemView.GRAVITY_LEFT;
            }
        } else if (i == len - 1) {
            {
                return SegmentItemView.GRAVITY_RIGHT;
            }
        } else {
            return SegmentItemView.GRAVITY_CENTER;
        }
    }

    public void setItems(String[] textItems) {
        this.mTextItems = textItems;
        init();
//        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        int childCount = getChildCount();
        int childWidthMeasureSpec = widthMeasureSpec;
        int maxWidth = 0;
        int maxHeight = 0;
        if (widthSize >= 0) {
            maxWidth = widthSize / childCount;
            childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(maxWidth, widthMode);
        }

        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            measureChild(child, childWidthMeasureSpec, heightMeasureSpec);
            maxWidth = Math.max(maxWidth, child.getMeasuredWidth());
            maxHeight = Math.max(maxHeight, child.getMeasuredHeight());

            System.out.println(" maxHeight== "  +  maxHeight);
            System.out.println(" child.getMeasuredHeight( == "  +  child.getMeasuredHeight());
        }

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(maxWidth * childCount, MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.EXACTLY);
        setMeasuredDimension(widthMeasureSpec,heightMeasureSpec);

//        setMeasuredDimension(getDefaultSize(maxWidth * childCount, widthMeasureSpec),
//                getDefaultSize(maxHeight, heightMeasureSpec));

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        if (!changed) return;
        int childCount = getChildCount();
        int left = 0;
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            child.layout(left, 0, left + child.getMeasuredWidth(), child.getMeasuredHeight());
            left += child.getMeasuredWidth();
        }
    }


    @Override
    public void onClick(View v) {

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (v.equals(child)) {
                checkedItem = i;
                ((SegmentItemView) child).setChecked(true);
            } else {
                ((SegmentItemView) child).setChecked(false);
            }
            child.postInvalidate();
        }
        if (listener != null) {
            listener.onItemClick((SegmentItemView) v, checkedItem);
        }
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(SegmentItemView item, int checkedItem);
    }
}

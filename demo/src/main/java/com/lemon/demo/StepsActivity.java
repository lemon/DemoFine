package com.lemon.demo;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lemon.stepview.StepView;

public class StepsActivity extends AppCompatActivity {

    private final String[] steps1 = {"Step 1", "Step 2", "Step 3"};

    private final String[] steps2 = {"Step 1", "Step 2", "Step 3", "Step 4"};

    private final String[] steps3 = {"Step 1", "Step 2", "Step 3", "Step 4", "Step 5"};

    private StepView mStepsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps);

        mStepsView = (StepView) findViewById(R.id.stepView);
        mStepsView.setLabels(steps1).setCompletedPosition(0);

        mStepsView = (StepView) findViewById(R.id.stepView2);
        mStepsView.setLabels(steps2).setCompletedPosition(2);

        mStepsView = (StepView) findViewById(R.id.stepView3);
        mStepsView.setLabels(steps3).setCompletedPosition(1);

    }
}
